/* MultiplePicker & SinglePicker */
declare type PickerOptionItem = { label: string; value: string | number } & { [key: string]: any };
