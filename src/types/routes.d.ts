declare type ChildRouteType = {
	path: string;
	name: string;
	component: any;
	meta: {
		icon?: string;
		title: string;
		noCache: boolean;
	};
};
