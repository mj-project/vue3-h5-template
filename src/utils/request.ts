import axios, { type AxiosInstance, type AxiosError, type AxiosResponse } from "axios";
import { showDialog, showToast, showFailToast } from "vant";
import { Local, Session } from "/@/utils/storage";
import { formatErrMsg } from "/@/utils/common";
import { refreshToken } from "/@/api/login";

// 白名单，不需要 token 的接口
const whiteList = ["/api/captcha/", "/api/login/"];

// 强制退出登录
const forceLogout = () => {
	Local.remove("token");
	Local.remove("refresh");
	Local.remove("userInfo");
	Session.clear();
	showDialog({
		title: "系统提示",
		message: "您的登录状态已过期，请重新登录！",
	}).then(() => window.location.reload());
};

// 刷新 token
const resetToken = async () => {
	try {
		const res = await refreshToken();
		if (res.access && res.refresh) {
			Local.set("token", res.access);
			Local.set("refresh", res.refresh);
			return Promise.resolve({ refresh: true, msg: "Token 刷新成功" });
		} else {
			forceLogout(); // refresh_token 过期，直接退出登录
			return Promise.resolve({ refresh: false, msg: "Token 刷新失败" });
		}
	} catch {
		forceLogout(); // 请求出错，直接退出登录
		return Promise.resolve({ refresh: false, msg: "Token 刷新请求出错" });
	}
};

// 新建一个 axios 实例
const service: AxiosInstance = axios.create({
	baseURL: import.meta.env.VITE_BASE_API,
	timeout: 1000 * 5,
	headers: { "Content-Type": "application/json" },
});

// 配置请求拦截器
service.interceptors.request.use(
	// 在发送请求之前做些什么
	config => {
		// if (config.url && !whiteList.includes(config.url)) {
		// 	if (Local.has("token")) {
		// 		config.headers.Authorization = `JWT ${Local.get("token")}`;
		// 	} else {
		// 		forceLogout();
		// 		return Promise.reject("token 不存在，请重新登录");
		// 	}
		// }
		return config;
	},
	// 处理请求错误
	(error: AxiosError) => {
		showFailToast(error.message);
		return Promise.reject(error);
	}
);

// 配置响应拦截器
service.interceptors.response.use(
	// 在 2xx 范围内的状态码都会触发该函数
	async (response: AxiosResponse) => {
		const { config, data } = response;
		// if (config.url !== "/api/token/refresh/" && data.code && data.code !== 200) {
		// 	if (data.msg && data.msg.code === "token_not_valid") {
		// 		const { refresh, msg } = await resetToken(); // token 过期，需要重新获取
		// 		if (refresh) {
		// 			return await service.request(config); // 重新发送请求，返回请求结果
		// 		} else {
		// 			showFailToast(msg);
		// 		}
		// 	} else {
		// 		showToast({ message: formatErrMsg(data.msg), position: "bottom" });
		// 	}
		// }
		return data;
	},
	// 超出 2xx 范围的状态码都会触发该函数
	(error: AxiosError) => {
		const { message, response } = error;
		if (message.indexOf("timeout") != -1) {
			showFailToast("网络超时");
		} else if (message == "Network Error") {
			showFailToast("网络连接错误");
		} else if (response?.status === 404) {
			showFailToast(`无法访问该接口: ${error.response?.config?.url}`);
		} else if (response?.status) {
			showFailToast(response.statusText);
		}
		return Promise.reject(error);
	}
);

// 导出 axios 实例
export default service;
