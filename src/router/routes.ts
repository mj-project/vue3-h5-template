import type { RouteRecordRaw } from "vue-router";
import Layout from "/@/layout/index.vue";

// Tabbar路由
export const tabbarRoutes: ChildRouteType[] = [
	{
		path: "/home",
		name: "Home",
		component: () => import("/@/views/home/index.vue"),
		meta: {
			icon: "wap-home-o",
			title: "主页",
			noCache: false,
		},
	},
	{
		path: "/tools",
		name: "Tools",
		component: () => import("/@/views/tools/index.vue"),
		meta: {
			icon: "gem-o",
			title: "工具",
			noCache: false,
		},
	},
];

// 子页面路由
export const pageRoutes: ChildRouteType[] = [
	{
		path: "/about",
		name: "About",
		component: () => import("/@/views/about/index.vue"),
		meta: {
			title: "二级页面",
			noCache: true,
		},
	},
	{
		path: "/:path(.*)*",
		name: "NotFound",
		component: () => import("/@/views/error/404.vue"),
		meta: {
			title: "找不到此页面",
			noCache: true,
		},
	},
];

// 总路由
export const routes: RouteRecordRaw[] = [
	{
		path: "/login",
		name: "Login",
		component: () => import("/@/views/login/index.vue"),
		meta: {
			title: "登录",
			noCache: true,
		},
	},
	{
		path: "/",
		name: "root",
		component: Layout,
		redirect: "/home",
		children: [...tabbarRoutes, ...pageRoutes],
	},
];
